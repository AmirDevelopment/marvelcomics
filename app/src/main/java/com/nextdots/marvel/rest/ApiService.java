package com.nextdots.marvel.rest;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;

import com.nextdots.marvel.BuildConfig;
import com.nextdots.marvel.UI.CircleTransform;
import com.nextdots.marvel.model.ComicListResponse;
import com.nextdots.marvel.model.Image;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by Amir Granadillo on 19/12/2016.
 */

public class ApiService implements IPictures, IComicService {
    private static Picasso picasso;
    private static RestAdapter mRestAdapter;
    private static MarvelService mMarvelService;

    private static final int LIMIT_COMICS = 30;
    private static final int MAX = 30;
    private static final int MIN = 1;
    private static final String PUBLIC_API_KEY = "828abd82ddbcb9a8ece878aac2646c70";
    private static final String PRIVATE_API_KEY = "341aa0ce603cd73dd3bda82dfd5c16e19e925d3a";
    private static final String PICTURE_SIZE_SMALL = "standard_small";
    private static final String PICTURE_SIZE_MEDIUM = "standard_medium";
    private static final String PICTURE_SIZE_LARGE = "standard_large";
    private static final String PICTURE_SIZE_FANTASTIC = "standard_fantastic";
    private static final String PICTURE_SIZE_XLARGE = "standard_xlarge";
    private static final String PICTURE_SIZE_AMAZING = "standard_amazing";
    private static final String PICTURE_SIZE_PORTRAIT_INCREDIBLE = "portrait_incredible";


    private static String API_URL = "https://gateway.marvel.com/v1/public/";

    static {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(5, TimeUnit.MINUTES);
        client.setReadTimeout(5, TimeUnit.MINUTES);
        if (BuildConfig.DEBUG) {
            client.networkInterceptors().add(new com.facebook.stetho.okhttp.StethoInterceptor());
        }

        mRestAdapter = new RestAdapter.Builder()
                .setClient(new OkClient(client))
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                .setEndpoint(API_URL).build();

        mMarvelService = mRestAdapter.create(MarvelService.class);
    }

    private void initPicasso(Context context, Picasso.Listener listener) {

        OkHttpClient client = new OkHttpClient();
        client.networkInterceptors().add(new com.facebook.stetho.okhttp.StethoInterceptor());

        if (picasso == null) {
            picasso = new Picasso.Builder(context)
                    .downloader(new OkHttpDownloader(context, Integer.MAX_VALUE))
                    .loggingEnabled(BuildConfig.DEBUG)
                    .indicatorsEnabled(BuildConfig.DEBUG)
                    .listener(listener)
                    .build();
            Picasso.setSingletonInstance(picasso);
        }
    }


    @Override
    public void loadPhoto(Context context, ImageView view, Image image, Picasso.Listener listener) {
        initPicasso(context, listener);
        Picasso.with(context)
                .load(image.getPath() + "/" + PICTURE_SIZE_PORTRAIT_INCREDIBLE + "." + image.getExtension())
                //  .networkPolicy(NetworkPolicy.OFFLINE)
                .fit()
                .into(view);
    }

    @Override
    public void loadUserPhoto(Context context, ImageView view, String url, Picasso.Listener listener) {
        initPicasso(context, listener);
        Picasso.with(context)
                .load(url.toString())
                .transform(new CircleTransform())
                //  .networkPolicy(NetworkPolicy.OFFLINE)
                .into(view);
    }

    @Override
    public void getRandomComicList(Callback<ComicListResponse> listCallback) {
        mMarvelService.getcomics(LIMIT_COMICS, new Random().nextInt(MAX - MIN + 1) + MIN, PUBLIC_API_KEY,
                getMD5(Long.toString(1) + PRIVATE_API_KEY + PUBLIC_API_KEY),
                1, null, listCallback);
        Log.i(this.getClass().getName() + "getRandomComicList()", " " + Long.toString(1) + PRIVATE_API_KEY + PUBLIC_API_KEY);
    }

    @Override
    public void getComicsByTitle(String title, Callback<ComicListResponse> listCallback) {
        mMarvelService.getcomics(LIMIT_COMICS, new Random().nextInt(MAX - MIN + 1) + MIN, PUBLIC_API_KEY,
                getMD5(Long.toString(1) + PRIVATE_API_KEY + PUBLIC_API_KEY),
                1, title, listCallback);
        Log.i(this.getClass().getName() + "getRandomComicList()", " " + Long.toString(1) + PRIVATE_API_KEY + PUBLIC_API_KEY);
    }

    public String getMD5(String text) {
        MessageDigest m = null;
        try {
            m = MessageDigest.getInstance("MD5");
            m.reset();
            m.update(text.getBytes());
            byte[] digest = m.digest();
            BigInteger bigInt = new BigInteger(1, digest);
            String hashtext = bigInt.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }
}
