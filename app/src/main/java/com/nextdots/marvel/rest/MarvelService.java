package com.nextdots.marvel.rest;

import com.nextdots.marvel.model.ComicListResponse;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;


/**
 * Created by Amir Granadillo on 19/12/2016.
 */

public interface MarvelService {

//    @GET("/customers")
//    void getCustomers(@Header("Authorization") String authorization,
//                      @Header("X-Company") String provider,
//                      Callback<List<Customer>> listCallback);

    //https://gateway.marvel.com/v1/public/comics?limit=30&offset=50&apikey=828abd82ddbcb9a8ece878aac2646c70&hash=0ce01b9121ea019c13164623edf348c0&ts=1

    @GET("/comics")
    void getcomics(@Query("limit") int limit,
                   @Query("offset") int offset,
                   @Query("apikey") String apikey,
                   @Query("hash") String hash,
                   @Query("ts") long timestamp,
                   @Query("titleStartsWith") String titleStartsWith,
                   Callback<ComicListResponse> listCallback);
}