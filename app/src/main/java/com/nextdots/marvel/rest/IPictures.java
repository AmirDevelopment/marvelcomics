package com.nextdots.marvel.rest;

import android.content.Context;
import android.widget.ImageView;

import com.nextdots.marvel.model.Image;
import com.squareup.picasso.Picasso;

/**
 * Created by Amir Granadillo on 19/12/2016.
 */

public interface IPictures {

    void loadPhoto(Context context, ImageView view, Image image, Picasso.Listener listener);

    void loadUserPhoto(Context context, ImageView view, String url,Picasso.Listener listener);

}
