package com.nextdots.marvel.rest;

import com.nextdots.marvel.model.ComicListResponse;

import retrofit.Callback;


/**
 * Created by Amir Granadillo on 19/12/2016.
 */

public interface IComicService {

    void getRandomComicList(Callback<ComicListResponse> listCallback);
    void getComicsByTitle(String title,Callback<ComicListResponse> listCallback);
}
