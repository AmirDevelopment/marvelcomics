package com.nextdots.marvel.mpvInterfaces;

import android.view.View;
import android.widget.ImageView;

import com.nextdots.marvel.model.Comic;
import com.nextdots.marvel.model.User;

import java.util.List;

/**
 * Created by Amir Granadillo on 18/12/2016.
 */

public interface IComicListView extends IBaseView {
    void showComics(List<Comic> comicList);

    void showComicDetail(Comic comic, View view);
    void fillSideMenuData(User user);

    void showIsFavorite(ImageView imageView, boolean isFavorite);
}
