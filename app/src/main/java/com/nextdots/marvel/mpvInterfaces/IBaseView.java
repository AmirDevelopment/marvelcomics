package com.nextdots.marvel.mpvInterfaces;

import android.content.Context;

/**
 * Created by Amir Granadillo on 18/12/2016.
 */

public interface IBaseView {
    Context getContext();

    void showLoading();

    void hideLoading();

    void showError(String message);

    void goToNextView();

}
