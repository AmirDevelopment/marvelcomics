package com.nextdots.marvel.mpvInterfaces;

import android.widget.ImageView;

import com.nextdots.marvel.model.Comic;
import com.nextdots.marvel.model.Image;

/**
 * Created by Amir Granadillo on 18/12/2016.
 */

public interface IComicDetailPresenter extends IBasePresenter{

    void loadComicPicture(ImageView imageView, Image image);

    void addFavorite(Comic comic);

    void checkIsFavorite(Comic comic);

    void showIsFavorite(boolean isfavorite);

}
