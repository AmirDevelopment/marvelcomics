package com.nextdots.marvel.mpvInterfaces;

import android.content.Intent;


/**
 * Created by Amir Granadillo on 18/12/2016.
 */

public interface ILoginView extends IBaseView {
    void showGoogleLoging(Intent intent);
}
