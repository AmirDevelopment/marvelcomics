package com.nextdots.marvel.mpvInterfaces;

import android.content.Context;
import android.content.Intent;

import com.facebook.login.widget.LoginButton;
import com.nextdots.marvel.DefaultActivity;

/**
 * Created by Amir Granadillo on 18/12/2016.
 */

public interface ILoginPresenter extends IBasePresenter {

    void launchGoogleLogin(DefaultActivity defaultActivity);

    void launchFacebookLogin(LoginButton button);

    void showGoogleView(Intent intent);

    void startListeningForGoogleSigningChanges();

    void stopListeningForGoogleSigningChanges();

    void sendGoogleCredentialsToFirebase(DefaultActivity defaultActivity, Intent intent);

    void hasSession(Context context);

    void sendFacebookCredentialsToFirebase(DefaultActivity defaultActivity, int requestCode, int resultCode, Intent data);


}
