package com.nextdots.marvel.mpvInterfaces;

/**
 * Created by Amir Granadillo on 18/12/2016.
 */

public interface IBasePresenter {
    void onSuccess();

    void onFailure(String error);
}
