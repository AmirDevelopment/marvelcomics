package com.nextdots.marvel.mpvInterfaces;

import android.content.Context;
import android.content.Intent;

import com.facebook.login.widget.LoginButton;
import com.nextdots.marvel.DefaultActivity;

/**
 * Created by Amir Granadillo on 18/12/2016.
 */

public interface ILoginInteractor {
    void signInWithGoogle(DefaultActivity defaultActivity);

    void signInWithFacebook(DefaultActivity defaultActivity, LoginButton loginButton);

    void sendFirebaseCredentialsToGoogle(DefaultActivity defaultActivity, Intent intent);

    void sendFirebaseCredentialsToFacebook(DefaultActivity defaultActivity, int requestCode, int resultCode, Intent data);

    void listenForGoogleSigningChanges();

    void stopListeningForGoogleSigningChanges();

    boolean checkSession(Context context);

}
