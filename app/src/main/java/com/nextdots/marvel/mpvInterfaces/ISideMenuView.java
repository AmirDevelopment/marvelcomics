package com.nextdots.marvel.mpvInterfaces;

import android.view.MenuItem;

import com.nextdots.marvel.model.User;

/**
 * Created by Amir Granadillo on 18/12/2016.
 */

public interface ISideMenuView extends IBaseView {
    void showMenu();

    void hideMenu();

    boolean onSideMenuSelectedItem(MenuItem menuItem);

    void setupSideMenu(User user);
}
