package com.nextdots.marvel.mpvInterfaces;

import android.widget.ImageView;

import com.nextdots.marvel.model.Comic;
import com.nextdots.marvel.model.Image;
import com.nextdots.marvel.model.User;

/**
 * Created by Amir Granadillo on 18/12/2016.
 */

public interface IComicListInteractor {

    User getUserData();

    void loadUserImage(ImageView imageView);

    void loadComicImage(ImageView imageView, Image image);

    void getRandomComics();

    void getComicByTitle(String title);

    void removeSession();

    void checkIsFavorite(ImageView imageView, Comic comic);




}
