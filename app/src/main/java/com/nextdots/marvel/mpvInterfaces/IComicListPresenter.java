package com.nextdots.marvel.mpvInterfaces;

import android.widget.ImageView;

import com.nextdots.marvel.model.Comic;
import com.nextdots.marvel.model.Image;

import java.util.List;

/**
 * Created by Amir Granadillo on 18/12/2016.
 */

public interface IComicListPresenter extends IBasePresenter {
    void getRandomComicList();

    void getComicListByTitle(String title);

    void onSucces(List<Comic> comicList);

    void getUserDataForSideMenu();

    void loadProfileImage(ImageView imageView);

    void loadComicPicture(ImageView imageView, Image image);

    void removeSession();

    void checkIsFavorite(ImageView imageView, Comic comic);

    void showIsFavorite(ImageView imageView, boolean isfavorite);

}
