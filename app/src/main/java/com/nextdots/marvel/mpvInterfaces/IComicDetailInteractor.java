package com.nextdots.marvel.mpvInterfaces;

import android.widget.ImageView;

import com.nextdots.marvel.model.Comic;
import com.nextdots.marvel.model.Image;

/**
 * Created by Amir Granadillo on 18/12/2016.
 */

public interface IComicDetailInteractor {

    void loadComicImage(ImageView imageView, Image image);

    void saveComic(Comic comic);

    void checkIsFavorite(Comic comic);



}
