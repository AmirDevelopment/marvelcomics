package com.nextdots.marvel.mpvInterfaces;

/**
 * Created by Amir Granadillo on 18/12/2016.
 */

public interface IComicDetailView extends IBaseView {
    void loadComicDetail();

    void showIsFavorite(boolean isfavorite);
}
