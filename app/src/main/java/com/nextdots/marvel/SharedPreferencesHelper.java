package com.nextdots.marvel;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nextdots.marvel.model.Comic;
import com.nextdots.marvel.model.User;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Amir Granadillo on 19/12/2016.
 */

public class SharedPreferencesHelper {
    public static final String SHARED_PREFERENCES_NAME = "marvelcomics";
    public static final String KEY_USER = "user";
    public static final String KEY_COMIC = "comicid:";

    public static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public static User getSessionUser(Context context) {
        Type type = new TypeToken<User>() {
        }.getType();
        return new Gson().fromJson(getSharedPreferences(context).getString(KEY_USER, ""), type);
    }

    public static void setSessionUser(Context context, User user) {
        getSharedPreferences(context).edit().putString(KEY_USER,
                new Gson().toJson(user)).commit();
    }

    public static void removeSessionUser(Context context) {
        getSharedPreferences(context).edit().remove(KEY_USER).commit();
    }

    public static List<Comic> getFavoriteComics(Context context) {
        Map<String, ?> allEntries = getSharedPreferences(context).getAll();
        List<Comic> comics = new ArrayList<>();
        Type type = new TypeToken<Comic>() {
        }.getType();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            if (entry.getKey().contains(KEY_COMIC)) {
                comics.add(new Gson().fromJson(entry.getValue().toString(), type));
            }

            Log.d("map values", entry.getKey() + ": " + entry.getValue().toString());
        }
        return comics;
    }

    public static void removeAllFavorites(Context context) {
        Map<String, ?> allEntries = getSharedPreferences(context).getAll();
        List<Comic> comics = new ArrayList<>();
        Type type = new TypeToken<Comic>() {
        }.getType();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            if (entry.getKey().contains(KEY_COMIC)) {
                removeFavorite(context, new Gson().fromJson(entry.getValue().toString(), type));
            }
        }
    }


    public static void addFavorite(Context context, Comic comic) {
        getSharedPreferences(context).edit().putString(KEY_COMIC + comic.getId(),
                new Gson().toJson(comic)).commit();
    }

    public static void removeFavorite(Context context, Comic comic) {
        getSharedPreferences(context).edit().remove(KEY_COMIC + comic.getId()).commit();
    }

    public static boolean isFavorite(Context context, Comic comic) {
        return getSharedPreferences(context).contains(KEY_COMIC + comic.getId());
    }
}
