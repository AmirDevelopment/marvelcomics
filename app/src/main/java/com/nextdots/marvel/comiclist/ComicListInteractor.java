package com.nextdots.marvel.comiclist;

import android.content.Context;
import android.widget.ImageView;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.nextdots.marvel.MarvelApplication;
import com.nextdots.marvel.R;
import com.nextdots.marvel.SharedPreferencesHelper;
import com.nextdots.marvel.model.Comic;
import com.nextdots.marvel.model.ComicListResponse;
import com.nextdots.marvel.model.Image;
import com.nextdots.marvel.model.User;
import com.nextdots.marvel.mpvInterfaces.IComicListInteractor;
import com.nextdots.marvel.mpvInterfaces.IComicListPresenter;
import com.nextdots.marvel.rest.ApiService;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by Amir Granadillo on 19/12/2016.
 */

public class ComicListInteractor implements IComicListInteractor {

    private IComicListPresenter mIComicListPresenter;

    public ComicListInteractor(IComicListPresenter iComicListPresenter) {
        mIComicListPresenter = iComicListPresenter;
    }

    @Override
    public User getUserData() {
        return SharedPreferencesHelper.getSessionUser(MarvelApplication.getInstance());
    }

    @Override
    public void loadUserImage(ImageView imageView) {
        ApiService apiService = new ApiService();
        Context context = MarvelApplication.getInstance();
        apiService.loadUserPhoto(context, imageView, SharedPreferencesHelper.getSessionUser(context)
                .getPicture(), (picasso, uri, exception) ->
                mIComicListPresenter.onFailure(MarvelApplication.getInstance()
                        .getString(R.string.error_loading_user_picture)));
    }

    @Override
    public void loadComicImage(ImageView imageView, Image image) {
        ApiService apiService = new ApiService();
        Context context = MarvelApplication.getInstance();
        apiService.loadPhoto(context, imageView, image,
                (picasso, uri, exception) ->
                        mIComicListPresenter.onFailure(MarvelApplication.getInstance()
                                .getString(R.string.error_loading_comic_picture)));
    }

    @Override
    public void getRandomComics() {
        ApiService apiService = new ApiService();
        apiService.getRandomComicList(new Callback<ComicListResponse>() {
            @Override
            public void success(ComicListResponse comicListResponse, Response response) {
                if (comicListResponse.getData().getResults() != null && comicListResponse.getData().getResults().size() > 0)
                    mIComicListPresenter.onSucces(comicListResponse.getData().getResults());
                else
                    mIComicListPresenter.onFailure(MarvelApplication.getInstance().getString(R.string.error_comic_not_found));
            }

            @Override
            public void failure(RetrofitError error) {
                mIComicListPresenter.onFailure(MarvelApplication.getInstance().getString(R.string.error_loading_comic));
            }
        });
    }

    @Override
    public void getComicByTitle(String title) {
        ApiService apiService = new ApiService();
        apiService.getComicsByTitle(title, new Callback<ComicListResponse>() {
            @Override
            public void success(ComicListResponse comicListResponse, Response response) {
                if (comicListResponse.getData().getResults() != null && comicListResponse.getData().getResults().size() > 0)
                    mIComicListPresenter.onSucces(comicListResponse.getData().getResults());
                else
                    mIComicListPresenter.onFailure(MarvelApplication.getInstance().getString(R.string.error_not_comic_title_found));
            }

            @Override
            public void failure(RetrofitError error) {
                mIComicListPresenter.onFailure(MarvelApplication.getInstance().getString(R.string.error_loading_comic));
            }
        });
    }

    @Override
    public void removeSession() {
        LoginManager.getInstance().logOut();
        FirebaseAuth.getInstance().signOut();
        SharedPreferencesHelper.removeSessionUser(MarvelApplication.getInstance());
    }

    @Override
    public void checkIsFavorite(ImageView imageView, Comic comic) {
        mIComicListPresenter.showIsFavorite(imageView, SharedPreferencesHelper.isFavorite(MarvelApplication.getInstance(), comic));

    }
}
