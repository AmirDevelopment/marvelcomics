package com.nextdots.marvel.comiclist;

import android.widget.ImageView;

import com.nextdots.marvel.model.Comic;
import com.nextdots.marvel.model.Image;
import com.nextdots.marvel.mpvInterfaces.IComicListInteractor;
import com.nextdots.marvel.mpvInterfaces.IComicListPresenter;
import com.nextdots.marvel.mpvInterfaces.IComicListView;

import java.util.List;

/**
 * Created by Amir Granadillo on 19/12/2016.
 */

public class ComicListPresenter implements IComicListPresenter {

    private IComicListView mIcomicListView;
    private IComicListInteractor mComicListInteractor;

    public ComicListPresenter(IComicListView iComicListView) {
        mIcomicListView = iComicListView;
        mComicListInteractor = new ComicListInteractor(this);
    }

    @Override
    public void getRandomComicList() {
        mComicListInteractor.getRandomComics();
    }

    @Override
    public void getComicListByTitle(String title) {
        mIcomicListView.showLoading();
        mComicListInteractor.getComicByTitle(title);
    }

    @Override
    public void onSucces(List<Comic> comicList) {
        mIcomicListView.hideLoading();
        mIcomicListView.showComics(comicList);
    }

    @Override
    public void getUserDataForSideMenu() {
        mIcomicListView.fillSideMenuData(mComicListInteractor.getUserData());
    }

    @Override
    public void loadProfileImage(ImageView imageView) {
        mComicListInteractor.loadUserImage(imageView);
    }

    @Override
    public void loadComicPicture(ImageView imageView, Image image) {
        mComicListInteractor.loadComicImage(imageView, image);

    }

    @Override
    public void removeSession() {
        mComicListInteractor.removeSession();
    }

    @Override
    public void checkIsFavorite(ImageView imageView, Comic comic) {
        mComicListInteractor.checkIsFavorite(imageView, comic);
    }

    @Override
    public void showIsFavorite(ImageView imageView, boolean isfavorite) {
        mIcomicListView.showIsFavorite(imageView, isfavorite);
    }


    @Override
    public void onSuccess() {

    }


    @Override
    public void onFailure(String error) {
        mIcomicListView.hideLoading();
        mIcomicListView.showError(error);
    }

}
