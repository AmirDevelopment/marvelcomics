package com.nextdots.marvel.comiclist;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nextdots.marvel.ComicsItemAdapter;
import com.nextdots.marvel.DefaultActivity;
import com.nextdots.marvel.R;
import com.nextdots.marvel.comicdetails.ComicDetailActivity;
import com.nextdots.marvel.favorites.FavoritesActivity;
import com.nextdots.marvel.login.LoginActivity;
import com.nextdots.marvel.model.Comic;
import com.nextdots.marvel.model.User;
import com.nextdots.marvel.mpvInterfaces.IComicListView;
import com.nextdots.marvel.mpvInterfaces.ISideMenuView;

import java.util.List;

import butterknife.BindView;

public class ComicListActivity extends DefaultActivity
        implements IComicListView, ISideMenuView {

    @BindView(R.id.nav_view)
    protected NavigationView mNavigationView;
    @BindView(R.id.drawer_layout)
    protected DrawerLayout mDrawerLayout;
    @BindView(R.id.toolbar)
    protected Toolbar mToolbar;
    @BindView(R.id.rv_comic_list)
    protected RecyclerView mRvComicList;
    private SearchView mSearchView;

    private boolean isSearching = false;
    private ComicsItemAdapter mComicsItemAdapter;

    private ComicListPresenter mComicListPresenter;

    public static void show(DefaultActivity activity) {
        activity.startActivity(new Intent(activity, ComicListActivity.class));
        activity.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_main);
    }

    /**
     * Default Methods
     */

    @Override
    protected void setup() {
        setSupportActionBar(mToolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(toggle);
        toggle.syncState();
        mNavigationView.setNavigationItemSelectedListener(item -> onSideMenuSelectedItem(item));
        mComicListPresenter = new ComicListPresenter(this);
        mComicListPresenter.getUserDataForSideMenu();
        StaggeredGridLayoutManager lm = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        mRvComicList.setLayoutManager(lm);

        mComicsItemAdapter = new ComicsItemAdapter(this, mComicListPresenter);
        mRvComicList.setAdapter(mComicsItemAdapter);
        showLoadingDialog();
        isSearching = true;
        mComicListPresenter.getRandomComicList();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_comic_list, menu);
        mSearchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!isSearching) {
                    isSearching = true;
                    mComicListPresenter.getComicListByTitle(query.trim());
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });


        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                showLoadingDialog();
                isSearching = true;
                mComicListPresenter.getRandomComicList();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Comic list view implementation
     */

    @Override
    public void showComics(List<Comic> comicList) {
        isSearching = false;
        hideLoadingDialog();
        if (mSearchView != null)
            mSearchView.clearFocus();
        hideKeyboard();
        mComicsItemAdapter.setComicList(comicList);
    }

    @Override
    public void showComicDetail(Comic comic, View view) {
        ComicDetailActivity.show(this, comic, view);
    }


    @Override
    public void fillSideMenuData(User user) {
        setupSideMenu(user);
    }

    @Override
    public void showIsFavorite(ImageView imageView, boolean isfavorite) {
        imageView.setVisibility(isfavorite ? View.VISIBLE : View.GONE);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        super.showLoadingDialog();
    }

    @Override
    public void hideLoading() {
        super.hideLoadingDialog();
    }

    @Override
    public void showError(String message) {
        isSearching = false;
        showToast(message);
    }

    @Override
    public void goToNextView() {

    }

    /**
     * Drawer menu methods
     */

    @Override
    public void showMenu() {

    }

    @Override
    public void hideMenu() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public boolean onSideMenuSelectedItem(MenuItem menuItem) {
        int id = menuItem.getItemId();

        if (id == R.id.favorite) {
            FavoritesActivity.show(this);

        } else if (id == R.id.close_session) {

            mComicListPresenter.removeSession();
            LoginActivity.show(this);

        }

        hideMenu();
        return true;
    }

    @Override
    public void setupSideMenu(User user) {
        View navigationHeader = mNavigationView.getHeaderView(0);
        ((TextView) navigationHeader.findViewById(R.id.nav_text_email)).setText(user.getEmail());
        ((TextView) navigationHeader.findViewById(R.id.nav_text_name)).setText(user.getName());
        mComicListPresenter.loadProfileImage((ImageView) navigationHeader.findViewById(R.id.nav_image));
    }
}
