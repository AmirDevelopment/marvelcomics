package com.nextdots.marvel.comicdetails;

import android.widget.ImageView;

import com.nextdots.marvel.model.Comic;
import com.nextdots.marvel.model.Image;
import com.nextdots.marvel.mpvInterfaces.IComicDetailInteractor;
import com.nextdots.marvel.mpvInterfaces.IComicDetailPresenter;
import com.nextdots.marvel.mpvInterfaces.IComicDetailView;

/**
 * Created by Amir Granadillo on 20/12/2016.
 */

public class ComicDetailPresenter implements IComicDetailPresenter {

    private IComicDetailView mIComicDetailView;
    private IComicDetailInteractor mIComicDetailInteractor;

    public ComicDetailPresenter(IComicDetailView iComicDetailView) {
        mIComicDetailView = iComicDetailView;
        mIComicDetailInteractor = new ComicDetailInteractor(this) ;

    }

    @Override
    public void onSuccess() {

    }

    @Override
    public void onFailure(String error) {
        mIComicDetailView.showError(error);
    }

    @Override
    public void loadComicPicture(ImageView imageView, Image image) {
        mIComicDetailInteractor.loadComicImage(imageView,image);
    }

    @Override
    public void addFavorite(Comic comic) {
        mIComicDetailInteractor.saveComic(comic);
    }

    @Override
    public void checkIsFavorite(Comic comic) {
        mIComicDetailInteractor.checkIsFavorite(comic);
    }

    @Override
    public void showIsFavorite(boolean isfavorite) {
        mIComicDetailView.showIsFavorite(isfavorite);
    }
}
