package com.nextdots.marvel.comicdetails;

import android.content.Context;
import android.widget.ImageView;

import com.nextdots.marvel.MarvelApplication;
import com.nextdots.marvel.R;
import com.nextdots.marvel.SharedPreferencesHelper;
import com.nextdots.marvel.model.Comic;
import com.nextdots.marvel.model.Image;
import com.nextdots.marvel.mpvInterfaces.IComicDetailInteractor;
import com.nextdots.marvel.mpvInterfaces.IComicDetailPresenter;
import com.nextdots.marvel.rest.ApiService;

/**
 * Created by Amir Granadillo on 20/12/2016.
 */

public class ComicDetailInteractor implements IComicDetailInteractor {

    private IComicDetailPresenter mIComicDetailPresenter;

    public ComicDetailInteractor(IComicDetailPresenter iComicDetailPresenter) {
        mIComicDetailPresenter = iComicDetailPresenter;
    }

    @Override
    public void loadComicImage(ImageView imageView, Image image) {
        ApiService apiService = new ApiService();
        Context context = MarvelApplication.getInstance();
        apiService.loadPhoto(context, imageView, image,
                (picasso, uri, exception) ->
                        mIComicDetailPresenter.onFailure(MarvelApplication.getInstance()
                                .getString(R.string.error_loading_comic_picture)));
    }

    @Override
    public void saveComic(Comic comic) {
        if (!SharedPreferencesHelper.isFavorite(MarvelApplication.getInstance(), comic)) {
            SharedPreferencesHelper.addFavorite(MarvelApplication.getInstance(), comic);
            mIComicDetailPresenter.showIsFavorite(true);
        } else {
            SharedPreferencesHelper.removeFavorite(MarvelApplication.getInstance(), comic);
            mIComicDetailPresenter.showIsFavorite(false);
        }
    }

    @Override
    public void checkIsFavorite(Comic comic) {
        mIComicDetailPresenter.showIsFavorite(SharedPreferencesHelper.isFavorite(MarvelApplication.getInstance(), comic));
    }
}
