package com.nextdots.marvel.comicdetails;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nextdots.marvel.DefaultActivity;
import com.nextdots.marvel.R;
import com.nextdots.marvel.model.Comic;
import com.nextdots.marvel.mpvInterfaces.IComicDetailPresenter;
import com.nextdots.marvel.mpvInterfaces.IComicDetailView;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Amir Granadillo on 20/12/2016.
 */

public class ComicDetailActivity extends DefaultActivity implements IComicDetailView {
    private static final String INTENT_COMIC = "com.nextdots.marvel.Comic";

    @BindView(R.id.comic_art)
    protected ImageView mComicArt;
    @BindView(R.id.toolbar)
    protected Toolbar mToolbar;
    @BindView(R.id.app_bar_layout)
    protected AppBarLayout appBarLayout;
    @BindView(R.id.collapsing_toolbar)
    protected CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.text_comic_title)
    protected TextView textComicTitle;
    @BindView(R.id.text_comic_description)
    protected TextView textComicDescription;
    @BindView(R.id.text_comic_price)
    protected TextView textComicPrice;
    @BindView(R.id.text_comic_date)
    protected TextView textComicDate;
    @BindView(R.id.text_comic_page_count)
    protected TextView textComicPageCount;
    @BindView(R.id.text_comic_series)
    protected TextView textComicSeries;
    @BindView(R.id.text_comic_creators)
    protected TextView textComicCreators;
    @BindView(R.id.text_comic_characters)
    protected TextView textComicCharacters;
    @BindView(R.id.fab)
    protected FloatingActionButton fab;

    private MenuItem menuItemAdd;

    private IComicDetailPresenter mIComicDetailPresenter;

    private Comic mComic;

    private boolean isCollapsed;

    public static void show(DefaultActivity activity, Comic comic, View view) {
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(activity, view, "comicArt");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            activity.startActivity(new Intent(activity, ComicDetailActivity.class).putExtra(INTENT_COMIC, comic), options.toBundle());
        } else {
            activity.startActivity(new Intent(activity, ComicDetailActivity.class).putExtra(INTENT_COMIC, comic));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_comic_detail);
    }

    @Override
    protected void setup() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getIntent().hasExtra(INTENT_COMIC))
            mComic = (Comic) getIntent().getExtras().getSerializable(INTENT_COMIC);
        mIComicDetailPresenter = new ComicDetailPresenter(this);
        mIComicDetailPresenter.loadComicPicture(mComicArt, mComic.getThumbnail());

        if (appBarLayout != null)
            appBarLayout.addOnOffsetChangedListener((layout, offset) -> {
                try {
                    mIComicDetailPresenter.checkIsFavorite(mComic);
                    isCollapsed = (float) Math.abs(offset) / (float) appBarLayout.getTotalScrollRange() >= 0.85;
                    if (menuItemAdd != null)
                        menuItemAdd.setVisible(isCollapsed);
                    collapsingToolbar.setTitle(isCollapsed ? getString(R.string.title_activity_comic_detail) : "");
                } catch (Throwable ignored) {
                }
            });

        textComicTitle.setText(mComic.getTitle());
        textComicDescription.setText(mComic.getDescription());
        textComicPrice.setText(mComic.getPrices().get(0).getPrice() > 0 ? "$" + Double.toString(mComic.getPrices().get(0).getPrice()) : "SOLD");
        textComicPrice.setTextColor(ContextCompat.getColor(this, mComic.getPrices().get(0).getPrice() > 0 ? R.color.gray : R.color.colorPrimary));
        textComicDate.setText(mComic.getDates().get(0).getDate());
        textComicPageCount.setText(Integer.toString(mComic.getPageCount()));
        textComicSeries.setText(mComic.getSeries().getName());
        textComicCreators.setText(mComic.getCreators().getAvailable() > 0 ? android.text.TextUtils.join(",", mComic.getCreators().getItems()) : "-----");
        textComicCharacters.setText(mComic.getCharacters().getAvailable() > 0 ? android.text.TextUtils.join(",", mComic.getCharacters().getItems()) : "-----");

        mIComicDetailPresenter.checkIsFavorite(mComic);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_comic_detail, menu);
        menuItemAdd = menu.findItem(R.id.action_add_favorite);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_add_favorite:
                mIComicDetailPresenter.addFavorite(mComic);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.fab)
    public void onClick() {
        mIComicDetailPresenter.addFavorite(mComic);
    }

    @Override
    public void onBackPressed() {
        supportFinishAfterTransition();
    }

    /**
     * Interface implementation
     */

    @Override
    public void loadComicDetail() {

    }

    @Override
    public void showIsFavorite(boolean isfavorite) {
        fab.setBackgroundColor(ContextCompat.getColor(this, isfavorite ? R.color.black : R.color.colorPrimary));
        fab.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, isfavorite ? R.color.yellow : R.color.colorPrimary)));
        if (menuItemAdd != null)
            menuItemAdd.setIcon(isfavorite ? R.drawable.ic_star_yellow : R.drawable.ic_star_white);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        super.showLoadingDialog();
    }

    @Override
    public void hideLoading() {
        super.hideLoadingDialog();
    }

    @Override
    public void showError(String message) {
        showToast(message);
    }

    @Override
    public void goToNextView() {

    }
}
