package com.nextdots.marvel;

import android.support.multidex.MultiDexApplication;

import com.facebook.stetho.Stetho;
import com.google.firebase.FirebaseApp;

/**
 * Created by Amir Granadillo on 19/12/2016.
 */

public class MarvelApplication extends MultiDexApplication {

    private static MarvelApplication mInstance;


    public static MarvelApplication getInstance() {
        return mInstance;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
        Stetho.initializeWithDefaults(this);
        mInstance = this;
    }
}
