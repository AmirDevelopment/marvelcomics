package com.nextdots.marvel.login;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.nextdots.marvel.DefaultActivity;
import com.nextdots.marvel.MarvelApplication;
import com.nextdots.marvel.R;
import com.nextdots.marvel.SharedPreferencesHelper;
import com.nextdots.marvel.model.User;
import com.nextdots.marvel.mpvInterfaces.ILoginInteractor;
import com.nextdots.marvel.mpvInterfaces.ILoginPresenter;

/**
 * Created by Amir Granadillo on 19/12/2016.
 */

public class LoginInteractor implements ILoginInteractor {

    private ILoginPresenter mILoginPresenter;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private GoogleApiClient mGoogleApiClient;
    GoogleSignInOptions gso;
    CallbackManager mCallbackManager;

    public LoginInteractor(ILoginPresenter iLoginPresenter) {
        this.mILoginPresenter = iLoginPresenter;

    }

    @Override
    public void signInWithGoogle(DefaultActivity defaultActivity) {
        if (mAuth == null)
            mAuth = FirebaseAuth.getInstance();
        if (mAuthListener == null)
            mAuthListener = firebaseAuth -> {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null && SharedPreferencesHelper.getSessionUser(MarvelApplication.getInstance().getApplicationContext()) == null) {

                    SharedPreferencesHelper.setSessionUser(MarvelApplication.getInstance().getApplicationContext(),
                            new User.Builder()
                                    .setId(user.getUid())
                                    .setPicture(user.getPhotoUrl().toString())
                                    .setName(user.getDisplayName())
                                    .setEmail(user.getEmail())
                                    .build());
                    mILoginPresenter.onSuccess();
                }
            };

        if (gso == null)
            gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(defaultActivity.getString(R.string.web_client_id))
                    .requestEmail()
                    .build();
        if (mGoogleApiClient == null)
            mGoogleApiClient = new GoogleApiClient.Builder(defaultActivity)
                    .enableAutoManage(defaultActivity, connectionResult ->
                            mILoginPresenter.onFailure(MarvelApplication.getInstance()
                                    .getString(R.string.error_google_ogin_failed)))
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        // this build the intent to show google fragment later
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        mILoginPresenter.showGoogleView(signInIntent);
    }

    @Override
    public void signInWithFacebook(DefaultActivity defaultActivity, LoginButton loginButton) {

        if (mAuth == null)
            mAuth = FirebaseAuth.getInstance();
        if (mAuthListener == null)
            mAuthListener = firebaseAuth -> {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null && SharedPreferencesHelper.getSessionUser(MarvelApplication.getInstance().getApplicationContext()) == null) {

                    SharedPreferencesHelper.setSessionUser(MarvelApplication.getInstance().getApplicationContext(),
                            new User.Builder()
                                    .setId(user.getUid())
                                    .setPicture(user.getPhotoUrl().toString())
                                    .setName(user.getDisplayName())
                                    .setEmail(user.getEmail())
                                    .build());
                    mILoginPresenter.onSuccess();
                }
            };

        AppEventsLogger.activateApp(MarvelApplication.getInstance());

        mCallbackManager = CallbackManager.Factory.create();

        loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AuthCredential credential = FacebookAuthProvider.getCredential(loginResult.getAccessToken().getToken());
                mAuth.signInWithCredential(credential)
                        .addOnCompleteListener(defaultActivity, task -> {
                            if (!task.isSuccessful()) {
                                mILoginPresenter.onFailure(task.getException().getMessage());
                                LoginManager.getInstance().logOut();

                            }
                            // ...
                        });
            }

            @Override
            public void onCancel() {
                // ...
                mILoginPresenter.onFailure(MarvelApplication.getInstance().getString(R.string.error_login_failed));
            }

            @Override
            public void onError(FacebookException error) {
                mILoginPresenter.onFailure(MarvelApplication.getInstance().getString(R.string.error_login_failed));

            }
        });
    }


    @Override
    public void sendFirebaseCredentialsToGoogle(DefaultActivity defaultActivity, Intent intent) {
        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(intent);
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            Log.d(LoginActivity.class.getName(), "firebaseAuthWithGoogle:" + acct.getId());
            AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
            mAuth.signInWithCredential(credential)
                    .addOnCompleteListener(defaultActivity, task -> {
                        Log.d(LoginActivity.class.getName(), "signInWithCredential:onComplete:" + task.isSuccessful());

                        if (!task.isSuccessful()) {
                            Log.w(LoginActivity.class.getName(), "signInWithCredential", task.getException());
                            mILoginPresenter.onFailure(task.getException().getMessage());

                        }
                    });
        } else {
            mILoginPresenter.onFailure(MarvelApplication.getInstance().getString(R.string.error_login_failed));
        }
    }

    @Override
    public void sendFirebaseCredentialsToFacebook(DefaultActivity defaultActivity, int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void listenForGoogleSigningChanges() {
        if (mAuth != null)
            mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void stopListeningForGoogleSigningChanges() {
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public boolean checkSession(Context context) {
        return SharedPreferencesHelper.getSessionUser(context) != null;
    }
}
