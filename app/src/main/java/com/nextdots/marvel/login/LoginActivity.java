package com.nextdots.marvel.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.facebook.FacebookSdk;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.SignInButton;
import com.nextdots.marvel.DefaultActivity;
import com.nextdots.marvel.R;
import com.nextdots.marvel.comiclist.ComicListActivity;
import com.nextdots.marvel.mpvInterfaces.ILoginPresenter;
import com.nextdots.marvel.mpvInterfaces.ILoginView;

import butterknife.BindView;
import butterknife.OnClick;

public class LoginActivity extends DefaultActivity implements ILoginView {

    static final int REQUEST_CODE_GOOGLE_SIGN_IN = 15;
    static final int REQUEST_CODE_FACEBOOK_SIGN_IN = 64206;


    @BindView(R.id.google_button)
    protected SignInButton buttonGoogle;
    @BindView(R.id.facebook_button)
    protected LoginButton buttonFacebook;

    private ILoginPresenter mILoginPresenter;

    public static void show(DefaultActivity activity) {
        activity.startActivity(new Intent(activity, LoginActivity.class));
        activity.finish();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        super.setContentView(R.layout.activity_login);
    }


    @Override
    public void onStart() {
        super.onStart();
        mILoginPresenter.startListeningForGoogleSigningChanges();
    }

    @Override
    public void onStop() {
        super.onStop();
        mILoginPresenter.stopListeningForGoogleSigningChanges();
    }

    @Override
    protected void setup() {
        buttonGoogle.setSize(SignInButton.SIZE_STANDARD);
        mILoginPresenter = new LoginPresenter(this);
        mILoginPresenter.hasSession(this);
    }

    @OnClick({R.id.google_button, R.id.facebook_button})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.google_button:
                mILoginPresenter.launchGoogleLogin(this);
                mILoginPresenter.startListeningForGoogleSigningChanges();
                break;
            case R.id.facebook_button:
                mILoginPresenter.launchFacebookLogin(buttonFacebook);
                mILoginPresenter.startListeningForGoogleSigningChanges();

                break;
        }


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        super.showLoadingDialog();
    }

    @Override
    public void hideLoading() {
        super.hideLoadingDialog();
    }

    @Override
    public void showError(String message) {
        showToast(message);
    }

    @Override
    public void goToNextView() {
        mILoginPresenter.stopListeningForGoogleSigningChanges();
        ComicListActivity.show(this);
    }

    @Override
    public void showGoogleLoging(Intent intent) {
        startActivityForResult(intent, REQUEST_CODE_GOOGLE_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_GOOGLE_SIGN_IN) {
            mILoginPresenter.sendGoogleCredentialsToFirebase(this, data);
        } else if (requestCode == REQUEST_CODE_FACEBOOK_SIGN_IN) {
            mILoginPresenter.sendFacebookCredentialsToFirebase(this, requestCode, resultCode, data);

        }
    }

}

