package com.nextdots.marvel.login;

import android.content.Context;
import android.content.Intent;

import com.facebook.login.widget.LoginButton;
import com.nextdots.marvel.DefaultActivity;
import com.nextdots.marvel.mpvInterfaces.ILoginInteractor;
import com.nextdots.marvel.mpvInterfaces.ILoginPresenter;
import com.nextdots.marvel.mpvInterfaces.ILoginView;

/**
 * Created by Amir Granadillo on 19/12/2016.
 */

public class LoginPresenter implements ILoginPresenter {
    private ILoginView mILoginView;
    private ILoginInteractor mILoginInteractor;


    public LoginPresenter(ILoginView iLoginView) {
        this.mILoginView = iLoginView;
        mILoginInteractor = new LoginInteractor(this);
    }

    @Override
    public void launchGoogleLogin(DefaultActivity defaultActivity) {
        mILoginView.showLoading();
        mILoginInteractor.signInWithGoogle(defaultActivity);
    }

    @Override
    public void launchFacebookLogin(LoginButton button) {
        mILoginView.showLoading();
        mILoginInteractor.signInWithFacebook((DefaultActivity) button.getContext(), button);
    }

    @Override
    public void showGoogleView(Intent intent) {
        mILoginView.showGoogleLoging(intent);
    }

    @Override
    public void startListeningForGoogleSigningChanges() {
        mILoginInteractor.listenForGoogleSigningChanges();
    }

    @Override
    public void stopListeningForGoogleSigningChanges() {
        mILoginInteractor.stopListeningForGoogleSigningChanges();
    }


    @Override
    public void sendGoogleCredentialsToFirebase(DefaultActivity defaultActivity, Intent intent) {
        mILoginInteractor.sendFirebaseCredentialsToGoogle(defaultActivity, intent);
    }

    @Override
    public void hasSession(Context context) {
        if(mILoginInteractor.checkSession(context))
            mILoginView.goToNextView();
    }

    @Override
    public void sendFacebookCredentialsToFirebase(DefaultActivity defaultActivity, int requestCode, int resultCode, Intent data) {
        mILoginInteractor.sendFirebaseCredentialsToFacebook(defaultActivity, requestCode, resultCode, data);
    }


    @Override
    public void onSuccess() {
        mILoginView.hideLoading();
        mILoginView.goToNextView();
    }

    @Override
    public void onFailure(String error) {
        mILoginView.hideLoading();
        mILoginView.showError(error);
    }


}
