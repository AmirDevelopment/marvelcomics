package com.nextdots.marvel.model;

import java.io.Serializable;

/**
 * Created by Amir Granadillo on 19/12/2016.
 */

public class User implements Serializable {
    private static final long serialVersionUID = 13;

    private String picture;
    private String id;
    private String name;
    private String email;

    public User(Builder builder) {
        this.picture = builder.picture;
        this.id = builder.id;
        this.name = builder.name;
        this.email = builder.email;
    }

    public String getPicture() {
        return picture;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public static final class Builder{
        private String picture;
        private String id;
        private String name;
        private String email;

        public Builder setPicture(String picture) {
            this.picture = picture;
            return this;
        }

        public Builder setId(String id) {
            this.id = id;
            return this;

        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setEmail(String email) {
            this.email = email;
            return this;
        }

        public User build(){
            return new User(this);
        }
    }
}
