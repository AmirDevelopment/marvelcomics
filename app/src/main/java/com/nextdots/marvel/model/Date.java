package com.nextdots.marvel.model;

import java.io.Serializable;

/**
 * Created by Amir Granadillo on 21/12/2016.
 */

public class Date implements Serializable {
    private static final long serialVersionUID = 13;
    private String date;
    private String type;

    public Date(String date, String type) {
        this.date = date;
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
