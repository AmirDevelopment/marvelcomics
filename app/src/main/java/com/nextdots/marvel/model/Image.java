package com.nextdots.marvel.model;

/**
 * Created by amirgb on 20/12/16.
 */

import java.io.Serializable;

/**
 * Created by Amir Granadillo on 19/12/2016.
 */

public class Image implements Serializable {
    private static final long serialVersionUID = 13;

    private String path;
    private String extension;

    public Image(String path, String extension) {
        this.path = path;
        this.extension = extension;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
}

