package com.nextdots.marvel.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Amir Granadillo on 21/12/2016.
 */

public class Details implements Serializable {
    private static final long serialVersionUID = 13;
    private List<Item> items;
    private int available;

    public Details(List<Item> items, int available) {
        this.items = items;
        this.available = available;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }
}
