package com.nextdots.marvel.model;

import java.io.Serializable;

/**
 * Created by Amir Granadillo on 19/12/2016.
 */

public class Price implements Serializable {
    private static final long serialVersionUID = 13;
    private double price;
    private String type;

    public Price(double price, String type) {
        this.price = price;
        this.type = type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
