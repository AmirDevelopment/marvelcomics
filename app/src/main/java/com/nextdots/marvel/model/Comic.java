package com.nextdots.marvel.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Amir Granadillo on 19/12/2016.
 */

public class Comic implements Serializable {
    private static final long serialVersionUID = 13;
    private String id;
    private String title;
    private List<Price> prices;
    private List<Image> images;
    private Image thumbnail;
    private String description;
    private Details creators;
    private Details characters;
    private Item series;
    private int pageCount;
    private List<Date> dates;

    public Comic(String id, String title, List<Price> prices, List<Image> images, Image thumbnail, String description, Details creators, Details characters, Item series, int pageCount, List<Date> dates) {
        this.id = id;
        this.title = title;
        this.prices = prices;
        this.images = images;
        this.thumbnail = thumbnail;
        this.description = description;
        this.creators = creators;
        this.characters = characters;
        this.series = series;
        this.pageCount = pageCount;
        this.dates = dates;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Price> getPrices() {
        return prices;
    }

    public void setPrices(List<Price> prices) {
        this.prices = prices;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public Image getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Image thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Details getCreators() {
        return creators;
    }

    public void setCreators(Details creators) {
        this.creators = creators;
    }

    public Details getCharacters() {
        return characters;
    }

    public void setCharacters(Details characters) {
        this.characters = characters;
    }

    public Item getSeries() {
        return series;
    }

    public void setSeries(Item series) {
        this.series = series;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public List<Date> getDates() {
        return dates;
    }

    public void setDates(List<Date> dates) {
        this.dates = dates;
    }
}
