package com.nextdots.marvel.model;

import java.io.Serializable;

/**
 * Created by Amir Granadillo on 21/12/2016.
 */

public class Item implements Serializable{
    private static final long serialVersionUID = 13;
    private String name;
    private String role;
    private String resourceUri;

    public Item(String name, String role, String resourceUri) {
        this.name = name;
        this.role = role;
        this.resourceUri = resourceUri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getResourceUri() {
        return resourceUri;
    }

    public void setResourceUri(String resourceUri) {
        this.resourceUri = resourceUri;
    }

    @Override
    public String toString() {
        return name;
    }
}
