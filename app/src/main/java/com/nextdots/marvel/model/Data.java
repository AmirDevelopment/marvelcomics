package com.nextdots.marvel.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by amirgb on 20/12/16.
 */

public class Data implements Serializable {
    private static final long serialVersionUID = 13;
    private List<Comic> results;

    public Data(List<Comic> results) {
        this.results = results;
    }

    public List<Comic> getResults() {
        return results;
    }

    public void setResults(List<Comic> results) {
        this.results = results;
    }
}
