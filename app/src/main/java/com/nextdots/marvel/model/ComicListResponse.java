package com.nextdots.marvel.model;

import java.io.Serializable;

/**
 * Created by Amir Granadillo on 19/12/2016.
 */

public class ComicListResponse implements Serializable{
    private static final long serialVersionUID = 13;
    private String status;
    private int code;
    private Data data;

    public ComicListResponse(String status, int code, Data data) {
        this.status = status;
        this.code = code;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
