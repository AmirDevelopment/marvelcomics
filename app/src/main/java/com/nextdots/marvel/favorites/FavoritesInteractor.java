package com.nextdots.marvel.favorites;

import android.content.Context;
import android.widget.ImageView;

import com.nextdots.marvel.MarvelApplication;
import com.nextdots.marvel.R;
import com.nextdots.marvel.SharedPreferencesHelper;
import com.nextdots.marvel.model.Comic;
import com.nextdots.marvel.model.Image;
import com.nextdots.marvel.model.User;
import com.nextdots.marvel.mpvInterfaces.IFavoriteInteractor;
import com.nextdots.marvel.mpvInterfaces.IFavoritePresenter;
import com.nextdots.marvel.rest.ApiService;

import java.util.List;

/**
 * Created by amirgb on 21/12/16.
 */

public class FavoritesInteractor implements IFavoriteInteractor {

    private IFavoritePresenter mIFavoritePresenter;

    public FavoritesInteractor(IFavoritePresenter iFavoritePresenter) {
        mIFavoritePresenter = iFavoritePresenter;
    }

    @Override
    public User getUserData() {
        return null;
    }

    @Override
    public void loadUserImage(ImageView imageView) {

    }

    @Override
    public void loadComicImage(ImageView imageView, Image image) {
        ApiService apiService = new ApiService();
        Context context = MarvelApplication.getInstance();
        apiService.loadPhoto(context, imageView, image,
                (picasso, uri, exception) ->
                        mIFavoritePresenter.onFailure(MarvelApplication.getInstance()
                                .getString(R.string.error_loading_comic_picture)));
    }

    @Override
    public void getRandomComics() {
        List<Comic> comics = SharedPreferencesHelper.getFavoriteComics(MarvelApplication.getInstance());

        if (comics != null && comics.size() > 0)
            mIFavoritePresenter.onSucces(SharedPreferencesHelper.getFavoriteComics(MarvelApplication.getInstance()));
        else
            mIFavoritePresenter.onFailure(MarvelApplication.getInstance().getString(R.string.error_loading_comic));
    }

    @Override
    public void getComicByTitle(String title) {

    }

    @Override
    public void removeSession() {

    }

    @Override
    public void checkIsFavorite(ImageView imageView, Comic comic) {

    }
}
