package com.nextdots.marvel.favorites;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.nextdots.marvel.ComicsItemAdapter;
import com.nextdots.marvel.DefaultActivity;
import com.nextdots.marvel.R;
import com.nextdots.marvel.comicdetails.ComicDetailActivity;
import com.nextdots.marvel.model.Comic;
import com.nextdots.marvel.model.User;
import com.nextdots.marvel.mpvInterfaces.IFavoritePresenter;
import com.nextdots.marvel.mpvInterfaces.IFavoriteView;

import java.util.List;

import butterknife.BindView;

/**
 * Created by amirgb on 21/12/16.
 */

public class FavoritesActivity extends DefaultActivity implements IFavoriteView {
    @BindView(R.id.toolbar)
    protected Toolbar mToolbar;
    @BindView(R.id.rv_comic_list)
    protected RecyclerView mRvComicList;


    private ComicsItemAdapter mComicsItemAdapter;

    private IFavoritePresenter mFavoritesPresenter;
    private boolean isSearching;

    public static void show(DefaultActivity activity) {
        activity.startActivity(new Intent(activity, FavoritesActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_favorites);
    }

    @Override
    protected void setup() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mFavoritesPresenter = new FavoritesPresenter(this);
        StaggeredGridLayoutManager lm = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        mRvComicList.setLayoutManager(lm);
        mComicsItemAdapter = new ComicsItemAdapter(this, mFavoritesPresenter);
        mRvComicList.setAdapter(mComicsItemAdapter);
        showLoadingDialog();
        isSearching = true;
        mFavoritesPresenter.getRandomComicList();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void showComics(List<Comic> comicList) {
        isSearching = false;
        hideLoadingDialog();
        mComicsItemAdapter.setComicList(comicList);
    }

    @Override
    public void showComicDetail(Comic comic, View view) {
        ComicDetailActivity.show(this, comic, view);
    }

    @Override
    public void fillSideMenuData(User user) {

    }

    @Override
    public void showIsFavorite(ImageView imageView, boolean isFavorite) {

    }

    @Override
    public Context getContext() {
        return null;
    }

    @Override
    public void showLoading() {
        super.showLoadingDialog();

    }

    @Override
    public void hideLoading() {
        super.hideLoadingDialog();
    }

    @Override
    public void showError(String message) {
        hideLoading();
        showToast(message);
    }

    @Override
    public void goToNextView() {

    }
}
