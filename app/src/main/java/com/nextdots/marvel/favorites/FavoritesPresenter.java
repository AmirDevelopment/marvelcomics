package com.nextdots.marvel.favorites;

import android.widget.ImageView;

import com.nextdots.marvel.model.Comic;
import com.nextdots.marvel.model.Image;
import com.nextdots.marvel.mpvInterfaces.IFavoriteInteractor;
import com.nextdots.marvel.mpvInterfaces.IFavoritePresenter;
import com.nextdots.marvel.mpvInterfaces.IFavoriteView;

import java.util.List;

/**
 * Created by amirgb on 21/12/16.
 */

public class FavoritesPresenter implements IFavoritePresenter {

    private IFavoriteView mIFavoriteView;
    private IFavoriteInteractor mIFavoriteInteractor;

    public FavoritesPresenter(IFavoriteView iFavoriteView) {
        mIFavoriteView = iFavoriteView;
        mIFavoriteInteractor = new FavoritesInteractor(this);
    }

    @Override
    public void getRandomComicList() {
        mIFavoriteInteractor.getRandomComics();
    }

    @Override
    public void getComicListByTitle(String title) {

    }

    @Override
    public void onSucces(List<Comic> comicList) {
        mIFavoriteView.showComics(comicList);
    }

    @Override
    public void getUserDataForSideMenu() {

    }

    @Override
    public void loadProfileImage(ImageView imageView) {

    }

    @Override
    public void loadComicPicture(ImageView imageView, Image image) {
        mIFavoriteInteractor.loadComicImage(imageView, image);

    }

    @Override
    public void removeSession() {

    }

    @Override
    public void checkIsFavorite(ImageView imageView, Comic comic) {

    }

    @Override
    public void showIsFavorite(ImageView imageView, boolean isfavorite) {

    }

    @Override
    public void onSuccess() {

    }

    @Override
    public void onFailure(String error) {
        mIFavoriteView.showError(error);
    }
}
