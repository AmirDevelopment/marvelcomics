package com.nextdots.marvel;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.support.annotation.LayoutRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import java.lang.reflect.Field;

import butterknife.ButterKnife;

/**
 * Created by Amir Granadillo on 18/12/2016.
 */

public abstract class DefaultActivity extends AppCompatActivity {

    private ProgressDialog mProgressDialog;

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setup();
    }

    protected abstract void setup();

    protected void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    protected void showLoadingDialog() {
        if (mProgressDialog == null || !mProgressDialog.isShowing())
            mProgressDialog = ProgressDialog.show(this, "Please wait...", "Loading", true);
    }

    protected void hideLoadingDialog() {
        if (mProgressDialog != null)
            mProgressDialog.dismiss();
    }

    public void showSnackBar(View view, int message) {
        Snackbar snackbar = Snackbar.make(view, getString(message), Snackbar.LENGTH_LONG);

        snackbar.show();
    }

    protected void hideKeyboard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    protected void forceOverflowMenu(){
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception ignored) {
        }
    }

}
