package com.nextdots.marvel;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nextdots.marvel.model.Comic;
import com.nextdots.marvel.mpvInterfaces.IComicListPresenter;
import com.nextdots.marvel.mpvInterfaces.IComicListView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Amir Granadillo on 19/12/2016.
 */

public class ComicsItemAdapter extends RecyclerView.Adapter<ComicsItemAdapter.ComicListViewHolder> {
    private List<Comic> comicList;
    private IComicListPresenter mIComicListPresenter;
    private IComicListView mIComicListView;

    public ComicsItemAdapter(IComicListView iComicListView,IComicListPresenter iComicListPresenter) {
        mIComicListPresenter = iComicListPresenter;
        mIComicListView = iComicListView;
    }

    public void setComicList(List<Comic> comicList) {
        this.comicList = comicList;
        notifyDataSetChanged();
    }

    @Override
    public ComicListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ComicListViewHolder vh = new ComicListViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_comic, parent, false));
        return vh;
    }

    @Override
    public void onBindViewHolder(ComicListViewHolder holder, int position) {
        mIComicListPresenter.loadComicPicture(holder.imgComicArt, comicList.get(position).getThumbnail());
        holder.mTextComicTittle.setText(comicList.get(position).getTitle());
        mIComicListPresenter.checkIsFavorite(holder.star, comicList.get(position));
        if (comicList.get(position).getPrices().get(0).getPrice() > 0) {
            holder.mTextPrice.setText(Html.fromHtml("<b>" + "$" + Double.toString(comicList.get(position).getPrices().get(0).getPrice()) + "</b>"));
            holder.mTextPrice.setTextColor(ContextCompat.getColor(holder.mTextPrice.getContext(), R.color.black));
        } else {
            holder.mTextPrice.setText(Html.fromHtml("<b>" + "SOLD" + "</b>"));
            holder.mTextPrice.setTextColor(ContextCompat.getColor(holder.mTextPrice.getContext(), R.color.colorPrimary));
        }
    }

    @Override
    public int getItemCount() {
        return comicList != null ? comicList.size() : 0;
    }

    public final class ComicListViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.comic_art)
        ImageView imgComicArt;
        @BindView(R.id.text_comic_title)
        TextView mTextComicTittle;
        @BindView(R.id.text_price)
        TextView mTextPrice;
        @BindView(R.id.star)
        ImageView star;

        public ComicListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.comic_art)
        public void onclick(View view){
            mIComicListView.showComicDetail(comicList.get(getAdapterPosition()), imgComicArt);
        }
    }
}
